/* eslint-disable prettier/prettier */
import React from 'react';
const env = {
    dev: 'dev',
    test: 'test',
    stg: 'stg',
    product: 'product',
    local: 'local',
};
const API_URL = {
    dev: 'https://jsonplaceholder.typicode.com/',
};
const currentEnv = env.dev;

export const BASE_URL = API_URL[currentEnv];
export const USER_TOKEN = 'USER_TOKEN';