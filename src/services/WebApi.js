import axios from 'axios';
import { Alert } from 'react-native';
import { BASE_URL } from './config';

export default class WebApi {

  // GET Request
  async get(url) {
    // console.log("API_TOKEN: ", API_TOKEN)
    return axios.get(url, {
      headers: {
        Accept: 'application/json',
      },
    });
  }


  // Get User
  getuser() {
    let url = BASE_URL + 'users/';
    return this.get(url, {
      headers: { 'Content-Type': 'application/json' },
    });
  }

  // Get Albums
  getAlbums(id) {
    let url = BASE_URL + 'albums?userId=' + id;
    return this.get(url, {
      headers: { 'Content-Type': 'application/json' },
    });
  }

  getPictures(id) {
    let url = BASE_URL + 'photos?albumId=' + id;
    return this.get(url, {
      headers: { 'Content-Type': 'application/json' },
    });
  }



}
