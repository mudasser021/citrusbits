
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from '../src/screens/Splash';
import Intro from '../src/screens/Intro';
import Home from '../src/screens/Home';
import Albums from '../src/screens/Albums';
import Pictures from './screens/Pictures';
import ZoomImage from './screens/ZoomImage';
import Map from './screens/Map';

const Stack = createStackNavigator();



class Router extends React.Component {

    render() {
        return (
            <NavigationContainer>
                <Stack.Navigator headerMode='none'>
                    <Stack.Screen name="SplashScreen" component={SplashScreen} />
                    <Stack.Screen name="Intro" component={Intro} />
                    <Stack.Screen name="Home" component={Home} />
                    <Stack.Screen name="Albums" component={Albums} />
                    <Stack.Screen name="Pictures" component={Pictures} />
                    <Stack.Screen name="ZoomImage" component={ZoomImage} />
                    <Stack.Screen name="Map" component={Map} />
                </Stack.Navigator>
            </NavigationContainer>
        );
    }
}
export default Router;