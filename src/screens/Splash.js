import React, {Component} from 'react';
import {View, StatusBar} from 'react-native';
import {COLORS} from '../res';


class Splash extends Component {
  componentDidMount = () => {
    this._navListener = this.props.navigation.addListener('focus', async () => {
      this.mounted = true;
      this.timer();
    });
  };

  componentWillUnmount = () => {
    this._navListener;
    this.mounted = false;
    clearTimeout(this.timer);
  };

  timer = () =>
    setTimeout(() => {
      this.moveToHomeScreen();
    }, 3000);


  async moveToHomeScreen() {
  this.props.navigation.replace('Intro');
  }

  render() {
    return (
      <View style={{flex: 1, backgroundColor: COLORS.MAIN}}>
        <StatusBar backgroundColor='white' barStyle='light-content' />
      </View>
    );
  }
}

export default Splash;
 
