import React, { PureComponent } from 'react';
import { View, Image, SafeAreaView, StyleSheet, TouchableOpacity, Text, Dimensions } from 'react-native';
import { List, Header } from '../components';
import { COLORS, IMAGES } from '../res';
import Spinner from 'react-native-loading-spinner-overlay';
import { Card } from "react-native-elements";
import WebApi from '../services/WebApi';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';


const { width, height } = Dimensions.get('screen');
const mapStyle = [
    {
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#212121"
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#757575"
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#212121"
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#757575"
            }
        ]
    },
    {
        "featureType": "administrative.country",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#9e9e9e"
            }
        ]
    },
    {
        "featureType": "administrative.land_parcel",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "administrative.locality",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#bdbdbd"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#757575"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#181818"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#616161"
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "color": "#1b1b1b"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#2c2c2c"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#8a8a8a"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#373737"
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#3c3c3c"
            }
        ]
    },
    {
        "featureType": "road.highway.controlled_access",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#4e4e4e"
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#616161"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#757575"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#000000"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#3d3d3d"
            }
        ]
    }
]
class Home extends PureComponent {
    constructor(props) {
        super(props);
        //setting default state
        this.state = {
            loader: true,
            users: [],
            region: {
                latitude: 37.0902,
                longitude: -95.7129,
                latitudeDelta: 0.01,
                longitudeDelta: 0.01,
            }
        };
    }

    componentDidMount() {
        this.getUserList();
    }

    async getUserList() {
        new WebApi().getuser().then((response) => {
            if (response.status == 200) {
                console.log('response', response);
                this.setState({ users: response.data, loader: false })
            }
        }).catch(error => {
            console.log('error.response in get Users', error)
            this.setState({ loader: false })
        })
    }

    render() {
        return (
            <SafeAreaView style={styles.main}>
                <Spinner
                    visible={this.state.loader}
                    color={COLORS.MAIN}
                />
                <Header title='Users' />
                <View
                    style={{
                        flex: 1,
                        backgroundColor: '#F4F6F8',
                    }}>

                    <List
                        data={this.state.users}
                        keyExtractor={(item, index) => index.toString()}
                        contentContainerStyle={{ paddingBottom: 10 }}
                        showsHorizontalScrollIndicator={false}
                        scrollEventThrottle={32}
                        pagingEnabled
                        renderItem={({ item }) => {
                            return (
                                <View >
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Albums', { item })}>
                                        <Card containerStyle={{ borderRadius: hp('1.7%') }}>
                                            <View style={styles.main} >
                                                <View style={styles.userInfoWrapper}>
                                                    <View style={styles.userInfoInnerWrapper}>
                                                        <View>
                                                            <Text style={{ color: '#a2a2a2' }}>Name</Text>
                                                            <Text style={{ color: 'black' }}>{item.name}</Text>

                                                            <Text style={{ color: '#a2a2a2', marginTop: 10 }}>Email</Text>
                                                            <Text style={{ color: 'black' }}>{item.email}</Text>
                                                        </View>
                                                        <View style={{ marginTop: hp('2%') }}>
                                                            <Text style={{ color: '#a2a2a2' }}>Address</Text>
                                                            <Text style={{ color: 'black' }}>{item.address.street}, {item.address.suite} {item.address.city}</Text>
                                                        </View>
                                                    </View>

                                                    <View style={styles.mapWrapper}>
                                                        <MapView style={styles.map}
                                                            scrollEnabled={false}
                                                            customMapStyle={mapStyle}
                                                            zoomControlEnabled={false}
                                                            zoomEnabled={false}
                                                            zoomTapEnabled={false}
                                                            initialRegion={{
                                                                latitude: item.address.geo.lat,
                                                                longitude: item.address.geo.lng,
                                                                latitudeDelta: 0.0922,
                                                                longitudeDelta: 0.0421,
                                                            }}>
                                                            <MapView.Marker
                                                                coordinate={{
                                                                    latitude: item.address.geo.lat,
                                                                    longitude: item.address.geo.lng,
                                                                }}
                                                                title={"title"}
                                                                description={"description"}
                                                            />
                                                        </MapView>

                                                    </View>

                                                </View>
                                                <TouchableOpacity onPress={() => this.props.navigation.navigate('Map', { lat: item.address.geo.lat, lng: item.address.geo.lng })}>
                                                    <Text style={{ color: 'blue', textDecorationLine: true, alignSelf: 'flex-end',paddingBottom:10 }}>View Map in full Screen</Text>
                                                </TouchableOpacity>

                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                                                    <View>
                                                        <Text style={{ color: '#a2a2a2' }}>Phone</Text>
                                                        <Text style={{ color: 'black' }}>{item.phone.split(' ')[0]}</Text>
                                                    </View>
                                                    <View>
                                                        <Text style={{ color: '#a2a2a2' }}>Website</Text>
                                                        <Text style={{ color: 'black' }}>{item.website}</Text>
                                                    </View>
                                                </View>
                                            </View>


                                        </Card>
                                    </TouchableOpacity>

                                </View>
                            );
                        }}
                    />
                </View>
            </SafeAreaView >
        );
    }
}
const styles = StyleSheet.create({
    main: {
        flex: 1,
    },
    userInfoWrapper: {
        flexDirection: 'row',
        width: wp('90%'),
        alignSelf: 'center',
        justifyContent: 'space-between'
    },
    userInfoInnerWrapper: {
        width: '45%',
        justifyContent: 'center',
        padding: 10,
        flexDirection: 'column'
    },
    mapWrapper: {
        width: '45%',
        justifyContent: 'center',
        alignItems: 'center',
        height: '75%',
        overflow: 'hidden',
    },
    markerWrapper: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    map: {
        ...StyleSheet.absoluteFill,
        borderRadius: 16
    },
});


export default Home;