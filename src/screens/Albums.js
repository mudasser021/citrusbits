import React, { PureComponent } from 'react';
import { View, SafeAreaView, StyleSheet, TouchableOpacity, Text, Dimensions, Alert } from 'react-native';
import { List, Header } from '../components';
import { COLORS } from '../res';
import Spinner from 'react-native-loading-spinner-overlay';
import { Card } from "react-native-elements";
import WebApi from '../services/WebApi';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const { width, height } = Dimensions.get('screen');

class Albums extends PureComponent {
    constructor(props) {
        super(props);
        //setting default state
        this.state = {
            loader: true,
            albums: []
        };
    }

    componentDidMount() {
        this.getAlbumList();
    }

    async getAlbumList() {
        new WebApi().getAlbums(
            this.props.route.params.item.id
        ).then((response) => {
            if (response.status == 200) {
                console.log('Albums response', response)
                this.setState({ albums: response.data, loader: false })
            }
        }).catch(error => {
            console.log('error.response in get Albums', error)
            Alert.alert('Something went wrong');
            this.setState({ loader: false })
        })
    }


    render() {
        return (
            <SafeAreaView style={{ flex: 1, }}>
                <Header title='Albums' backgroundColor={COLORS.MAIN} backButton={true} navigation={this.props.navigation} />
                <Spinner
                    visible={this.state.loader}
                    color={COLORS.MAIN}
                />

                <View
                    style={{
                        flex: 1,
                        backgroundColor: '#F4F6F8',
                    }}>
                    <List
                        data={this.state.albums}
                        keyExtractor={(item, index) => index.toString()}
                        contentContainerStyle={{ paddingBottom: 10 }}
                        showsHorizontalScrollIndicator={false}
                        scrollEventThrottle={32}
                        pagingEnabled
                        renderItem={({ item }) => {
                            console.log('items Length: ', item)
                            return (
                                <View>
                                    <TouchableOpacity onPress={() => this.props.navigation.navigate('Pictures', { item })}>
                                        <Card containerStyle={{ borderRadius: hp('1.7%') }}>
                                            <Text style={{ marginLeft: wp('4%'), marginTop: hp('1%'), fontSize: 18, fontWeight: 'bold' }}>{item.title}</Text>
                                        </Card>
                                    </TouchableOpacity>

                                </View>
                            );
                        }}
                    />
                </View>
            </SafeAreaView >
        );
    }
}
const styles = StyleSheet.create({
    headerTitle: {
        fontSize: 22,
        color: 'black',
        fontWeight: 'bold'
    },
    seeAllTextStyle: {
        fontSize: 12,
        color: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    },
    searchBarStyle: {
        width: wp('80%'),
        height: hp('5%'),
        justifyContent: 'center',
    },
    paragraph: {
        margin: 24,
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#34495e',
    },
});



export default Albums;