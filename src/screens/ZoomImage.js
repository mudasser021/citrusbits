import React, { PureComponent } from 'react';
import { Image, Dimensions, View, SafeAreaView } from 'react-native';
import ImageZoom from 'react-native-image-pan-zoom';
import { Header } from '../components';
import { COLORS } from '../res';

const dimensions = Dimensions.get('window');
const imageHeight = Math.round(dimensions.width * 9 / 12);
const imageWidth = dimensions.width;

export default class App extends PureComponent {
    render() {
        return (
            <SafeAreaView>
                <View>
                    <Header title=''/>
                    <ImageZoom
                        cropWidth={Dimensions.get('window').width}
                        cropHeight={Dimensions.get('window').height}
                        imageWidth={imageWidth}
                        imageHeight={Dimensions.get('window').height}>
                        <Image style={{ height: Dimensions.get('window').height, width: imageWidth }}
                            source={{ uri: this.props.route.params.url }} />
                    </ImageZoom>
                </View>
            </SafeAreaView>
        )
    }
}