import React, { PureComponent } from 'react';
import { View, Image, SafeAreaView, StyleSheet, TouchableOpacity, Text, Dimensions, Alert } from 'react-native';
import { List } from '../components';
import { COLORS, IMAGES } from '../res';
import Spinner from 'react-native-loading-spinner-overlay';
import { Card, registerCustomIconType } from "react-native-elements";
import WebApi from '../services/WebApi';
import { Header } from '../components';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';

const { width, height } = Dimensions.get('screen');

class Pictures extends PureComponent {
    constructor(props) {
        super(props);
        //setting default state
        this.state = {
            loader: true,
            pictures: []
        };
    }

    componentDidMount() {
        this.getPictures();
    }

    async getPictures() {
        new WebApi().getPictures(
            this.props.route.params.item.userId
        ).then((response) => {
            if (response.status == 200) {
                this.setState({ pictures: response.data, loader: false })
            }
        }).catch(error => {
            console.log('error.response in get Albums', error)
            Alert.alert('Something went wrong');
            this.setState({ loader: false })
        })
    }

    imageZoom = (url) => {
        this.props.navigation.navigate('ZoomImage', { url })
        // console.log('url: ',url)
    }

    render() {
        return (
            <SafeAreaView style={{ flex: 1, }}>
                <Header title='Photos' backButton={true} navigation={this.props.navigation} />
                <Spinner
                    visible={this.state.loader}
                    color={COLORS.MAIN}
                />
                <View
                    style={styles.listWrapper}>
                    <List
                        data={this.state.pictures}
                        keyExtractor={(item, index) => index.toString()}
                        contentContainerStyle={{ paddingBottom: 10 }}
                        showsHorizontalScrollIndicator={false}
                        scrollEventThrottle={32}
                        bounces={false}
                        pagingEnabled
                        numColumns={2}
                        renderItem={({ item }) => {
                            return (
                                <View>
                                    <TouchableOpacity onPress={() => this.imageZoom(item.url)}>
                                        <Card containerStyle={styles.cardStyle}>
                                            <View >
                                                <Image
                                                    style={styles.imageDimension}
                                                    resizeMode='stretch'
                                                    source={{ uri: item.thumbnailUrl }}
                                                />
                                            </View>
                                        </Card>
                                    </TouchableOpacity>
                                    <Text style={styles.albumTitle}>{item.title}</Text>
                                </View>
                            );
                        }}
                    />
                </View>
            </SafeAreaView >
        );
    }
}
const styles = StyleSheet.create({
    imageDimension: {
        height: 160,
         width: 160
    },
    cardStyle: {
        padding: 0,
        width: 160,
        height: 160,
        justifyContent: 'center',
        alignItems: 'center'
    },
    listWrapper: {
        flex: 1,
        backgroundColor: '#F4F6F8',
        justifyContent: 'center',
        alignItems: 'center'
    },
    albumTitle: {
        marginLeft: 15,
        marginTop: 5,
        fontSize: 14,
        width: 160
    },
    headerTitle: {
        fontSize: 22,
        color: 'black',
        fontWeight: 'bold'
    },
    seeAllTextStyle: {
        fontSize: 12,
        color: 'black',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 10
    },
    searchBarStyle: {
        width: wp('80%'),
        height: hp('5%'),
        justifyContent: 'center',
    },
    paragraph: {
        margin: 24,
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#34495e',
    },
});



export default Pictures;