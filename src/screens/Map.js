import React, { useState } from 'react';
import MapView, { Marker } from "react-native-maps";
import { Header } from '../components';
import {View, SafeAreaView} from 'react-native';


const App = (props) => {
    console.log('props:: ',props.route.params.lat)
    const [region, setRegion] = useState({
        latitude: props.route.params.lat,
        longitude: props.route.params.lng,
        latitudeDelta: 0.009,
        longitudeDelta: 0.009
    });

    return (
        // <View>
        //      <Header title='Users' />
            <MapView
                style={{ flex: 1 }}
                region={region}
                onRegionChangeComplete={region => setRegion(region)}
            >
                <Marker coordinate={{ latitude: props.route.params.lat, longitude: props.route.params.lng }} />
            </MapView>
        // </View>

    );
};

export default App;