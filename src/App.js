import React, { Component } from 'react'
import Router from './Router';
import SplashScreen from 'react-native-splash-screen'

export default class App extends Component {
  async componentDidMount() {
    SplashScreen.hide();
  }

  render() {
    return (
      <Router />
    );
  }
}