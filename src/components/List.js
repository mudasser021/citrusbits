/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import { FlatList } from 'react-native';

const List = props => {
  return (
    <FlatList
      contentContainerStyle={props.contentContainerStyle}
      keyboardShouldPersistTaps={props.keyboardShouldPersistTaps}
      data={props.data}
      removeClippedSubviews
      bounces={props.bounces}
      renderItem={props.renderItem}
      ItemSeparatorComponent={props.ItemSeparatorComponent}
      showsVerticalScrollIndicator={props.showsVerticalScrollIndicator}
      horizontal={props.horizontal}
      showsHorizontalScrollIndicator={props.showsHorizontalScrollIndicator}
      numColumns={props.numColumns}
      snapToInterval={props.interval}
      keyExtractor={props.keyExtractor}
      ListEmptyComponent={props.ListEmptyComponent}
      onEndReached={props.onEndReached}
      onEndReachedThreshold={0.5}
    />
  );
};

export { List };
