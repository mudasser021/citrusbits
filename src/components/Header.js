import React from 'react';
import {SafeAreaView, View, Text, Image, TouchableOpacity, Platform} from 'react-native';
import {COLORS, IMAGES} from '../res';

const Header = props => {
  return (
    <SafeAreaView
      style={{
        flexDirection: 'row',
        width: '100%',
        height: Platform.OS == 'ios' ? 60 : 56,
        backgroundColor: COLORS.MAIN,
      }}>
      {props.backButton ? (
        <TouchableOpacity
          onPress={() => props.navigation.goBack()}
          style={{
            width: '20%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Image source={IMAGES.BACK_ARROW} style={{tintColor:'white'}} />
        </TouchableOpacity>
      ) : (
        <View
          style={{
            width: '20%',
            justifyContent: 'center',
            alignItems: 'center',
          }}>
        </View>
      )}

      <View
        style={{width: '60%', justifyContent: 'center', alignItems: 'center'}}>
        <Text style={{fontSize: 16, color: 'white', fontWeight: 'bold'}}>
          {props.title}
        </Text>
      </View>
    </SafeAreaView>
  );
};

export {Header};
