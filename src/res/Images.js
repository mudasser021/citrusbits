const IMAGES = {
  BACK_ARROW: require('./images/back-arrow.png'),
  MARKER: require('./images/marker.png'),
};

export { IMAGES };
